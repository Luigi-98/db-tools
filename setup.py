import setuptools

setuptools.setup(
    name="Db Tools",
    version="1.0",
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
    install_requires=[
        'pandas',
        'SQLAlchemy'
    ]
)
