from ._abstract import AbstractDatabase
from sqlalchemy.engine import URL


class MsSQLDatabase(AbstractDatabase):
    def _compute_uri_(self) -> str:
        self.DB_PORT = int(self.DB_PORT)
        if self.DB_PORT != 1433:
            raise ValueError(
                "Different ports for this MsSQL class are not allowed. Please use the port 1433."
            )
        connection_url = URL.create(
            "mssql+pymssql",
            username=self.DB_USER,
            password=self.DB_PWD,
            host=self.DB_HOST,
            # port=self.DB_PORT,  # if you add port, it doesn't work
            database=self.DB_NAME,
        )
        return connection_url

    def fast_readsql(self, *args, **kwargs):
        return super().fast_readsql(*args, **kwargs)

    def add_column(self, table_name, column_sql_statement):
        return super().add_column(table_name, column_sql_statement)

    def create_schema(self, schema_name, if_not_exists=True, authorization_user=None):
        return super().create_schema(schema_name, if_not_exists, authorization_user)

    def create_table(
        self, table_name, columns_sql_statement, if_not_exists=True, schema=None
    ):
        return super().create_table(
            table_name, columns_sql_statement, if_not_exists, schema
        )

    def insert_with_id(self, table_name, series, idx_column_name):
        return super().insert_with_id(table_name, series, idx_column_name)

    def remove_table(self, table_name, schema=None, if_exists=True):
        return super().remove_table(table_name, schema, if_exists)

    def remove_column(self, table_name: str, column_name: str, if_exists: bool = False):
        return super().remove_column(table_name, column_name, if_exists)
