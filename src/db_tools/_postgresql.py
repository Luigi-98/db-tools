import tempfile

import pandas as pd
from ._abstract import AbstractDatabase


class PostgresDatabase(AbstractDatabase):
    def _compute_uri_(self) -> str:
        return "postgresql+psycopg2://" + \
            self.DB_USER + ":" + self.DB_PWD + \
            "@" + self.DB_HOST + ":" + self.DB_PORT + "/" + self.DB_NAME

    def fast_readsql(self, query):
        connected_here = False
        if self.connection is None:
            self.connect()
            connected_here = True
        dtypes = next(self.pd_readsql(query, chunksize=10)).dtypes.to_dict()
        parse_dates_cols = [ col for col, dtype in dtypes.items() if dtype == 'datetime64[ns]' ]
        dtypes = { key: val for key, val in dtypes.items() if key not in parse_dates_cols }
        conn = self.connection
        with tempfile.TemporaryFile() as tmpfile:
            copy_sql = "COPY ({query}) TO STDOUT WITH CSV {head}".format(
                query=query, head="HEADER"
            )
            cur = conn.connection.cursor()
            cur.copy_expert(copy_sql, tmpfile)
            tmpfile.seek(0)
            df = pd.read_csv(tmpfile, low_memory=False, dtype=dtypes, parse_dates=parse_dates_cols)
        if connected_here:
            self.close()
        return df

    def remove_table(self, table_name, schema=None, if_exists=True):
        if_exists = "IF EXISTS" if if_exists else ""
        schema = "" if schema is None else (schema + ".")
        return self.query(f"DROP TABLE {if_exists} {schema}{table_name} CASCADE")

    def create_schema(
        self, schema_name, if_not_exists=True, authorization_user=None
    ):
        if_not_exists = "IF NOT EXISTS" if if_not_exists else ""
        sql_statement = f"CREATE SCHEMA {if_not_exists} {schema_name}"
        return self.query(sql_statement)

    def create_table(
        self, table_name, columns_sql_statement,
        if_not_exists=True, schema=None
    ):
        columns_sql_statement = [
            statement for statement in columns_sql_statement
            if statement.strip() != ""
        ]
        if_not_exists = "IF NOT EXISTS" if if_not_exists else ""
        schema = (schema + ".") if schema is not None else ""
        return self.query(
            f"CREATE TABLE {if_not_exists} {schema}{table_name} ("
            "" + ",\n".join(columns_sql_statement) + ");"
        )

    def insert_with_id(self, table_name, series, idx_column_name='idx'):
        if type(series) is pd.Series:
            col_names = series.index.apply(str)
            values = series.apply(str)
        elif type(series) is dict:
            col_names = list(series.keys())
            values = [ str(series[col]) for col in col_names ]
            col_names = [ str(col) for col in col_names ]
        else:
            raise TypeError("insert_with_id requires dict or pd.Series")

        sql_column_names = "(" + ",".join(col_names) + ")"
        sql_values = "('" + "', '".join(values) + "')"
        return self.query(
            f"""INSERT INTO {table_name}
                    {sql_column_names}
                VALUES
                    {sql_values}
                RETURNING {idx_column_name}
            """
        ).scalar()

    def add_column(self, table_name, column_sql_statement):
        query = f"""ALTER TABLE {table_name}
                    ADD COLUMN {column_sql_statement}"""
        return self.query(query)

    def remove_column(self, table_name, column_name, if_exists=False):
        if_exists = "IF EXISTS" if if_exists else ""
        query = f"""ALTER TABLE {table_name}
                    DROP COLUMN {if_exists} {column_name}"""
        return self.query(query)
