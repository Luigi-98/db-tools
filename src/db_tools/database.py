from ._postgresql import PostgresDatabase
from ._mssql import MsSQLDatabase
from ._abstract import AbstractDatabase


def Database(
    DB_USER, DB_PWD, DB_HOST, DB_PORT, DB_NAME, db_type="postgresql"
) -> AbstractDatabase:
    db_types = {
        "postgresql": PostgresDatabase,
        "mssql": MsSQLDatabase,
    }

    try:
        assert db_type in db_types
    except AssertionError:
        raise ValueError(f"Db type {db_type} not implemented")

    return db_types[db_type](
        DB_USER=DB_USER,
        DB_PWD=DB_PWD,
        DB_HOST=DB_HOST,
        DB_PORT=DB_PORT,
        DB_NAME=DB_NAME,
    )
