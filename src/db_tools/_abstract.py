from abc import ABC, abstractmethod
from typing import Any, Dict, Iterable, Union
import pandas as pd

from sqlalchemy import create_engine


class AbstractDatabase(ABC):
    connection = None

    def __init__(
        self,
        DB_USER: str,
        DB_PWD: str,
        DB_HOST: str,
        DB_PORT: Union[str, int],
        DB_NAME: str,
    ):
        self.DB_USER = DB_USER
        self.DB_PWD = DB_PWD
        self.DB_HOST = DB_HOST
        self.DB_PORT = DB_PORT
        self.DB_NAME = DB_NAME

        db_uri = self._compute_uri_()

        self.engine = create_engine(db_uri)

    def connect(self) -> None:
        self.connection = self.engine.connect()

    def close(self) -> None:
        if self.connection is not None:
            self.connection.close()
            self.connection = None

    def pd_readsql(self, *args, **kwargs) -> pd.DataFrame:
        connected_here = False
        if self.connection is None:
            self.connect()
            connected_here = True
        kwargs["con"] = self.connection
        res = pd.read_sql(*args, **kwargs)
        if connected_here:
            self.close()
        return res

    def pd_to_sql(self, *args, **kwargs) -> None:
        df: pd.DataFrame = kwargs.pop("df")
        connected_here = False
        if self.connection is None:
            self.connect()
            connected_here = True
        kwargs["con"] = self.connection
        if "if_exists" not in kwargs:
            kwargs["if_exists"] = "append"
        if "method" not in kwargs:
            kwargs["method"] = "multi"
        if "index" not in kwargs:
            kwargs["index"] = False
        df.to_sql(*args, **kwargs)
        if connected_here:
            self.close()

    def query(self, query: str) -> pd.DataFrame:
        connected_here = False
        if self.connection is None:
            self.connect()
            connected_here = True
        res = self.connection.execute(query)
        if connected_here:
            self.close()
        return res

    def insert(self, table_name: str, df: pd.DataFrame) -> None:
        return self.pd_to_sql(name=table_name, df=df)

    @abstractmethod
    def _compute_uri_(self) -> str:
        ...

    @abstractmethod
    def fast_readsql(self, query: str) -> pd.DataFrame:
        ...

    @abstractmethod
    def remove_table(self, table_name: str, schema: str = None, if_exists: bool = True):
        ...

    @abstractmethod
    def create_schema(
        self,
        schema_name: str,
        if_not_exists: bool = True,
        authorization_user: str = None,
    ):
        ...

    @abstractmethod
    def create_table(
        self,
        table_name: str,
        columns_sql_statement: Iterable[str],
        if_not_exists: bool = True,
        schema: str = None,
    ):
        ...

    @abstractmethod
    def insert_with_id(
        self,
        table_name: str,
        series: Union[pd.Series, Dict],
        idx_column_name: str,
    ) -> Any:
        ...

    @abstractmethod
    def add_column(
        self,
        table_name: str,
        column_sql_statement: str,
    ):
        ...

    @abstractmethod
    def remove_column(self, table_name: str, column_name: str, if_exists: bool = False):
        ...
